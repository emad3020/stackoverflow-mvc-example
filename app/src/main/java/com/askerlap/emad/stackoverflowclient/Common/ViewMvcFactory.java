package com.askerlap.emad.stackoverflowclient.Common;


import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.askerlap.emad.stackoverflowclient.Screens.QuestionsListViewMvc;
import com.askerlap.emad.stackoverflowclient.Screens.QuestionsListViewMvcImpI;
import com.askerlap.emad.stackoverflowclient.Screens.questionlist.QuestionListItemViewMvc;
import com.askerlap.emad.stackoverflowclient.Screens.questionlist.QuestionListItemViewMvcImpI;

public class ViewMvcFactory {

    private final LayoutInflater mLayoutInflater;

    public ViewMvcFactory(LayoutInflater mLayoutInflater) {
        this.mLayoutInflater = mLayoutInflater;
    }


    public QuestionsListViewMvc getQuestionsListViewMvc(@Nullable ViewGroup parent) {
        return new QuestionsListViewMvcImpI(mLayoutInflater,parent, this);
    }

    public QuestionListItemViewMvc getQuestionListItemViewMvc(@Nullable ViewGroup parent){
        return new QuestionListItemViewMvcImpI(mLayoutInflater,parent);

    }
}

package com.askerlap.emad.stackoverflowclient.Screens;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;

import com.askerlap.emad.stackoverflowclient.Common.BaseObservableViewMvc;
import com.askerlap.emad.stackoverflowclient.Common.ViewMvcFactory;
import com.askerlap.emad.stackoverflowclient.Model.Question;
import com.askerlap.emad.stackoverflowclient.QuestionsListAdapter;
import com.askerlap.emad.stackoverflowclient.R;

import java.util.List;


public class QuestionsListViewMvcImpI extends BaseObservableViewMvc<QuestionsListViewMvc.Listener>
        implements QuestionsListAdapter.onQuestionClickListener, QuestionsListViewMvc {

    private ListView mListQuestions;
    private QuestionsListAdapter mQuestionsListAdapter;
    public QuestionsListViewMvcImpI(LayoutInflater inflater , ViewGroup parent, ViewMvcFactory viewMvcFactory) {
        setRootView(inflater.inflate(R.layout.activity_main,parent,false));

        mListQuestions = findViewById(R.id.lst_questions);

        mQuestionsListAdapter = new QuestionsListAdapter(getContext(),this,viewMvcFactory);
        mListQuestions.setAdapter(mQuestionsListAdapter);

    }


    @Override
    public void onQuestionClicked(Question question) {

        for (Listener listener : getListener()) {
            listener.onQuestionClicked(question);
        }
    }

    @Override
    public void bindQuestions(List<Question> questions) {
        mQuestionsListAdapter.clear();
        mQuestionsListAdapter.addAll(questions);
        mQuestionsListAdapter.notifyDataSetChanged();
    }
}

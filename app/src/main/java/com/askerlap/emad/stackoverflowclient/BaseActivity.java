package com.askerlap.emad.stackoverflowclient;

import android.support.v7.app.AppCompatActivity;

import com.askerlap.emad.stackoverflowclient.Common.depencyInjection.CompositRoot;
import com.askerlap.emad.stackoverflowclient.Common.depencyInjection.ControllerCompositionRoot;

public class BaseActivity extends AppCompatActivity {

    private ControllerCompositionRoot mControllerCompositionRoot;

    protected ControllerCompositionRoot getCompositeRoot() {

        if (mControllerCompositionRoot == null) {
            mControllerCompositionRoot = new ControllerCompositionRoot(
                    ((CustomApplication) getApplication()).getmCompositRoot(),
                    this
            );
        }
        return mControllerCompositionRoot;
    }
}

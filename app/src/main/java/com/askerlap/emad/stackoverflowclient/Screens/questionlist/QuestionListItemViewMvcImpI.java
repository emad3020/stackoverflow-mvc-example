package com.askerlap.emad.stackoverflowclient.Screens.questionlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.askerlap.emad.stackoverflowclient.Common.BaseObservableViewMvc;
import com.askerlap.emad.stackoverflowclient.Model.Question;
import com.askerlap.emad.stackoverflowclient.R;

public class QuestionListItemViewMvcImpI  extends BaseObservableViewMvc<QuestionListItemViewMvc.Listener>
        implements QuestionListItemViewMvc {


    private final TextView mTxtTitle;

    private Question mQuestion;

    public QuestionListItemViewMvcImpI(LayoutInflater inflater , ViewGroup parent){
         setRootView(inflater.inflate(R.layout.layout_question_list_item, parent, false));
         mTxtTitle = findViewById(R.id.txt_title);

        getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for ( Listener listener : getListener()) {
                    listener.onQuestionClicked(mQuestion);
                }
            }
        });

    }



    @Override
    public void bindQuestion(Question question) {

        mQuestion = question;
        mTxtTitle.setText(question.getTitle());
    }
}

package com.askerlap.emad.stackoverflowclient;

import android.app.Application;


import com.askerlap.emad.stackoverflowclient.Common.depencyInjection.CompositRoot;

public class CustomApplication extends Application {

    private CompositRoot mCompositRoot ;

    @Override
    public void onCreate() {
        super.onCreate();

        mCompositRoot = new CompositRoot();
    }


    public CompositRoot getmCompositRoot() {
        return mCompositRoot;
    }
}

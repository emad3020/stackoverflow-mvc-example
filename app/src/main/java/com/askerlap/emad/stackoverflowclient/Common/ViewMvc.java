package com.askerlap.emad.stackoverflowclient.Common;

import android.view.View;

public interface ViewMvc {

    View getRootView();
}

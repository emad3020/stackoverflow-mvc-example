package com.askerlap.emad.stackoverflowclient.Screens;

import com.askerlap.emad.stackoverflowclient.Common.ObservableViewMvc;
import com.askerlap.emad.stackoverflowclient.Model.Question;

import java.util.List;

public interface QuestionsListViewMvc  extends ObservableViewMvc<QuestionsListViewMvc.Listener> {



    void bindQuestions(List<Question> questions);

    public interface Listener{
        void onQuestionClicked(Question question);
    }
}

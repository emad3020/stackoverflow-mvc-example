package com.askerlap.emad.stackoverflowclient;

import com.askerlap.emad.stackoverflowclient.Model.QuestionsListResponseSchema;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StackoverflowApi {


    //2.2/questions/featured?pagesize=20&order=desc&sort=activity&site=stackoverflow
    @GET("questions/featured?key = " + Constants.STACKOVERFLOW_API_KEY
            +"&order=desc&sort=activity&site=stackoverflow")

    Call<QuestionsListResponseSchema> fetchLastActiveQuestions(@Query("pagesize") int pageSize);
}

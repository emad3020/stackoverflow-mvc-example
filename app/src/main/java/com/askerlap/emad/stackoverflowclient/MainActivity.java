package com.askerlap.emad.stackoverflowclient;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.askerlap.emad.stackoverflowclient.Model.Question;
import com.askerlap.emad.stackoverflowclient.Model.QuestionSchema;
import com.askerlap.emad.stackoverflowclient.Model.QuestionsListResponseSchema;
import com.askerlap.emad.stackoverflowclient.Screens.QuestionsListViewMvc;
import com.askerlap.emad.stackoverflowclient.Screens.QuestionsListViewMvcImpI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity implements QuestionsListViewMvcImpI.Listener {
    private StackoverflowApi mStackoverflowApi;


    private QuestionsListViewMvc mViewMvc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mViewMvc = getCompositeRoot().getViewMvcFactory().getQuestionsListViewMvc(null);

        setContentView(mViewMvc.getRootView());

        mViewMvc.registerListener(this);


        mStackoverflowApi = getCompositeRoot().getStackoverflowApi();
    }

    @Override
    protected void onStart() {
        super.onStart();

        fetchQuestions();

    }

    private void fetchQuestions() {
        mStackoverflowApi.fetchLastActiveQuestions(Constants.QUESTIONS_LIST_PAGE_SIZE)
                .enqueue(new Callback<QuestionsListResponseSchema>() {
                    @Override
                    public void onResponse(Call<QuestionsListResponseSchema> call, Response<QuestionsListResponseSchema> response) {
                        Log.e("TAG" , response.body().getQuestions().size()+"");

                        if (response.isSuccessful()) {
                            bindQuestions(response.body().getQuestions());
                        } else {
                            networkCallFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<QuestionsListResponseSchema> call, Throwable t) {

                        networkCallFailed();
                    }
                });



    }

    private void networkCallFailed() {
        Toast.makeText(this,"Network call Failed",Toast.LENGTH_LONG).show();
    }

    private void bindQuestions(List<QuestionSchema> questionSchemas) {

        List<Question> questions = new ArrayList<>();
        for (QuestionSchema questionSchema : questionSchemas) {
            questions.add(new Question(questionSchema.getId(),questionSchema.getTitle()));
        }

        mViewMvc.bindQuestions(questions);

    }

    @Override
    public void onQuestionClicked(Question question) {

        Toast.makeText(this,question.getTitle(),Toast.LENGTH_LONG).show();
    }
}




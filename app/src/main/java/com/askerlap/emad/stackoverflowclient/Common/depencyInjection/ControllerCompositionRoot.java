package com.askerlap.emad.stackoverflowclient.Common.depencyInjection;

import android.app.Activity;
import android.view.LayoutInflater;

import com.askerlap.emad.stackoverflowclient.Common.ViewMvcFactory;
import com.askerlap.emad.stackoverflowclient.StackoverflowApi;

public class ControllerCompositionRoot {

    private final CompositRoot mCompositRoot;
    private final Activity activity;

    public ControllerCompositionRoot(CompositRoot mCompositRoot , Activity activity) {
        this.mCompositRoot = mCompositRoot;
        this.activity = activity;
    }


    public StackoverflowApi getStackoverflowApi() {
        return mCompositRoot.getStackoverflowApi();
    }

    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(activity);
    }

    public ViewMvcFactory getViewMvcFactory() {
        return new ViewMvcFactory(getLayoutInflater());
    }
}

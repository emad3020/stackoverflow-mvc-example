package com.askerlap.emad.stackoverflowclient;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.askerlap.emad.stackoverflowclient.Common.ViewMvcFactory;
import com.askerlap.emad.stackoverflowclient.Model.Question;
import com.askerlap.emad.stackoverflowclient.Screens.questionlist.QuestionListItemViewMvc;
import com.askerlap.emad.stackoverflowclient.Screens.questionlist.QuestionListItemViewMvcImpI;

public class QuestionsListAdapter extends ArrayAdapter<Question> implements QuestionListItemViewMvc.Listener {

    private final onQuestionClickListener mOnQuestionClickListener;
    private ViewMvcFactory viewMvcFactory;


    public interface onQuestionClickListener {
        void onQuestionClicked(Question question);
    }

    public QuestionsListAdapter(Context context, onQuestionClickListener onQuestionClickListener, ViewMvcFactory viewMvcFactory) {
        super(context,0);
        this.mOnQuestionClickListener = onQuestionClickListener;
        this.viewMvcFactory = viewMvcFactory;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {

            QuestionListItemViewMvc viewMvc = viewMvcFactory.getQuestionListItemViewMvc(parent);
            /*QuestionListItemViewMvc viewMvc = new QuestionListItemViewMvcImpI(
                    LayoutInflater.from(getContext()) ,
                    parent
            );*/

            viewMvc.registerListener(this);
            convertView = viewMvc.getRootView();
            convertView.setTag(viewMvc);
        }

        final Question question = getItem(position);

        QuestionListItemViewMvc viewMvc = (QuestionListItemViewMvc) convertView.getTag();

        viewMvc.bindQuestion(question);


        return convertView;
    }


    @Override
    public void onQuestionClicked(Question question) {

        mOnQuestionClickListener.onQuestionClicked(question);
    }

}

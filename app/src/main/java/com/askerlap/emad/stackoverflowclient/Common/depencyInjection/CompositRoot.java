package com.askerlap.emad.stackoverflowclient.Common.depencyInjection;

import com.askerlap.emad.stackoverflowclient.Constants;
import com.askerlap.emad.stackoverflowclient.StackoverflowApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompositRoot {
    private Retrofit mRetorfit;

    public StackoverflowApi getStackoverflowApi() {
        return getRetrofit().create(StackoverflowApi.class);
    }

    private Retrofit getRetrofit() {
        if (mRetorfit == null) {

            mRetorfit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return mRetorfit;
    }
}

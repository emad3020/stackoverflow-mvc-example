package com.askerlap.emad.stackoverflowclient.Screens.questionlist;

import com.askerlap.emad.stackoverflowclient.Common.ObservableViewMvc;
import com.askerlap.emad.stackoverflowclient.Model.Question;

public interface QuestionListItemViewMvc extends ObservableViewMvc<QuestionListItemViewMvc.Listener> {

    public interface Listener{
        void onQuestionClicked(Question question);
    }


    void bindQuestion(Question question);
}
